import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './App.css';
import { connect } from 'react-redux';
import {
  Segment,
  Menu,
  Container,
  Popup,
  Icon,
  Button,
  Form,
  Select } from 'semantic-ui-react';

import {
  SET_FILTER,
  SET_URL,
  SET_DATE,
  GET_OPTIONS
} from './constants/actionTypes';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
// import ReactTaj from 'react-taj';
import ReactTaj from './components/react-taj';
import { DateRangePicker } from 'react-dates';

class App extends Component {

  constructor() {
    super();
    this.state = {
      visible: false,
    };
    this.handleChange = (name, value) => this.props.handleChange(name, value);
    this.handleChangeDate = (startDate, endDate) => this.props.handleChangeDate(startDate, endDate);
    this.handleChangeUrl = ev => this.props.handleChangeUrl(ev.target.value);
    this.getOptions = (option, value) =>this.props.getOptions(option, value);
  }

  toggleVisibility = () => this.setState({
    visible: !this.state.visible
  })
  copyTextToClipboard = text => {
    const textArea = document.createElement('textarea');
    textArea.value = text;
    document.body.appendChild(textArea);

    textArea.select();

    try {
      const successful = document.execCommand('copy');
      const msg = successful
        ? 'successful'
        : 'unsuccessful';
      console.log(`Copying text command was ${msg}`);
    } catch (err) {
      console.log('Oops, unable to copy');
    }

    document.body.removeChild(textArea);
  }
  handleClick = () => {
    this.copyTextToClipboard(this.props.resultUrl);
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  UNSAFE_componentWillMount() {
    this.getOptions('filter-options1.json', 'filter1');
    this.getOptions('filter-options2.json', 'filter2');
    this.getOptions('filter-options3.json', 'filter3');
    this.getOptions('filter-options4.json', 'filter4');
    this.getOptions('filter-options5.json', 'filter5');
    this.getOptions('filter-options6.json', 'filter6');
  }
  render() {
    const {
      activeItem,
      visible } = this.state;

    return (<div>
      <Menu inverted={true}>
        <Menu.Item
          name='TAJ Dashboard'
          active={activeItem === 'TAJ'}
          onClick={this.handleItemClick}/>
        <Menu.Item
          name='home'
          active={activeItem === 'home'}
          onClick={this.handleItemClick}/>
      </Menu>
      <div className='taj__container'>
        <div className={visible?'taj__controllers':'taj__controllers taj__controllers--hide'}>
          <Container className={'controls'}>
            <Form onSubmit={this.handleSubmit}>
              <Form.Field>
                <label>{'Test URL'}</label>
                <Form.Input
                  placeholder='URL'
                  name='url'
                  required={true}
                  onChange={this.handleChangeUrl}
                  />
              </Form.Field>

              <Form.Field>
                <label>{'Date Range'}</label>
                <DateRangePicker
                  startDateId='startDate'
                  endDateId='endDate'
                  startDate={this.state.startDate}
                  endDate={this.state.endDate}
                  isOutsideRange={() => false}
                  onDatesChange={({ startDate, endDate }) => {
                    this.setState({ startDate, endDate });
                    this.handleChangeDate(startDate, endDate)
                  }}
                  focusedInput={this.state.focusedInput}
                  onFocusChange={focusedInput => {
                     this.setState({ focusedInput });
                  }}/>
              </Form.Field>
              <Form.Field
                name='filter1'
                search
                onChange={(name, value)=>{this.handleChange(value.name, value.value)}}
                control={Select} label='Filter 1'
                options={this.props.options.filter1}
                placeholder='Filter 1'/>
              <Form.Field
                name='filter2'
                search
                onChange={(name, value)=>{this.handleChange(value.name, value.value)}}
                control={Select} label='Filter 2'
                options={this.props.options.filter2}
                placeholder='Filter 2'/>
              <Form.Field
                name='filter3'
                search
                onChange={(name, value)=>{this.handleChange(value.name, value.value)}}
                control={Select} label='Filter 3'
                options={this.props.options.filter3}
                placeholder='Filter 3'/>
              <Form.Field
                name='filter4'
                search
                onChange={(name, value)=>{this.handleChange(value.name, value.value)}}
                control={Select} label='Filter 4'
                options={this.props.options.filter4}
                placeholder='Filter 4'/>
              <Form.Field
                name='filter5'
                search
                onChange={(name, value)=>{this.handleChange(value.name, value.value)}}
                control={Select} label='Filter 5'
                options={this.props.options.filter5}
                placeholder='Filter 5'/>
              <Form.Field
                name='filter6'
                search
                onChange={(name, value)=>{this.handleChange(value.name, value.value)}}
                control={Select} label='Filter 6'
                options={this.props.options.filter6}
                placeholder='Filter 6'/>
              <Form.Field>
                <label>{'Result URL'}</label>
                <Form.Input name='url' action={<Popup
                  trigger = {
                    <Button color='teal' icon='copy' onClick={this.handleClick}/>
                  }
                  content = 'copied'
                  on = 'click'
                  inverted
                />} value={this.props.resultUrl} placeholder='result url'/>
              </Form.Field>
            </Form>
          </Container>
        </div>
        <div className='taj__table'>
          <Button className='taj__control-btn' onClick={this.toggleVisibility}>
            <Icon className={visible?'btn_arrow':'btn_arrow arrow_right'} name='arrow left'/>
          </Button>
          <Segment basic={true}>
            <ReactTaj
              url={this.props.resultUrl}
              pagination={false}
              searchable={true}
              exportable={true}
              useBackgroundColors={true}
              useForegroundColors={true}/>
          </Segment>
        </div>
      </div>

    </div>);
  }
}
App.propTypes = {
  filters: PropTypes.object,
  handleChange:PropTypes.func,
  handleChangeUrl:PropTypes.func,
  getOptions:PropTypes.func,
  handleChangeDate:PropTypes.func,
  endTime: PropTypes.string,
  startTime: PropTypes.string,
  resultUrl: PropTypes.string,
  url: PropTypes.string,
  error: PropTypes.string,
  isFetching: PropTypes.bool,
  options: PropTypes.object,
};

const mapStateToProps = state => {
  return {
    filters: state.dashboard.filters,
    error: state.dashboard.error,
    isFetching: state.dashboard.isFetching,
    endTime: state.dashboard.endTime,
    startTime: state.dashboard.startTime,
    resultUrl: state.dashboard.resultUrl,
    url: state.dashboard.url,
    options: state.dashboard.options
  };
};

const mapDispatchToProps = dispatch => ({
  handleChange:(name, value) => 
    dispatch({type:SET_FILTER, subTitle: name, value: value}),
  handleChangeUrl: value => 
    dispatch({type:SET_URL, value: value}),
  getOptions: (url, option) =>
    dispatch({type:GET_OPTIONS, url: url, option:option}),
  handleChangeDate: (startDate, endDate) =>
    dispatch({type:SET_DATE, startDate: startDate, endDate: endDate})
  
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
