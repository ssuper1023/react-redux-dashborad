export const SET_URL = Symbol('SET_URL');
export const SET_FILTER = Symbol('SET_FILTER');
export const SET_DATE = Symbol('SET_DATE');
export const GET_OPTIONS = Symbol('GET_OPTIONS');


export const FETCH_OPTIONS_START = Symbol('FETCH_OPTIONS_START');
export const FETCH_OPTIONS_FAILURE = Symbol('FETCH_OPTIONS_FAILURE');
export const FETCH_OPTIONS_SUCCESS = Symbol('FETCH_OPTIONS_SUCCESS');
