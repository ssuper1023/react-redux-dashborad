import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import 'react-table/react-table.css';
import { Provider } from 'react-redux';
import configureStore from './configure-store.js';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'));
registerServiceWorker();
