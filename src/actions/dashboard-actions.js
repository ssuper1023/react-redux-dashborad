import {
  SET_FILTER,
  SET_URL
} from '../constants/actionTypes';


export const setFilter = (name, val) => {
	return {
		type: SET_FILTER,
		subTitle: name,
		value: val
	};
};

export const setURL = url => {
	return {
		type: SET_URL,
		payload: url
	};
};