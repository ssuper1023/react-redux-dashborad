const reshapeDate= date => {
  if (date) {
    date = JSON.stringify(date);
    const year = date.split('T')[0].substr(1);
    return year;
  }
};

module.exports = {
  reshapeDate
};
