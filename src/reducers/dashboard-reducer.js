import {
  SET_FILTER,
  SET_URL,
  SET_DATE,
  GET_OPTIONS
} from '../constants/actionTypes';
import { reshapeDate } from '../helpers.js';
const qs = require('qs');
const appendQuery = require('append-query');

const initialState = {
  isFetching: false,
  filters: {},
  options: {},
  endTime: '',
  startTime: '',
  resultUrl: 'http://localhost:5000/iris',
  url: 'http://localhost:5000/iris',
  error: undefined
};

function getOptionsFromApiAsync(url) {
   return fetch(url)
   .then((response) => response.json())
   .then((result) => {return result})
   .catch((error) => {
     console.error(error);
   });
}


const dashboard = (state = initialState, action) => {
  switch (action.type) {
    case SET_FILTER: {
      let { filters,endTime,startTime,resultUrl,url } = state;
      
      let name = action.subTitle;
      let value = action.value;

      filters[name] = value;

      let newFilters = filters;
      let qsUrl;
      if (value === 'op0') {
        value = null;
      }
      if (value !== null) {
        newFilters = {
          ...filters,
          [name]: value
        };
      } else {
        delete newFilters[name];
      }
      filters = newFilters;
      if (startTime) {
        qsUrl = qs.stringify({
          ...newFilters,
          startTime,
          endTime
        });
        resultUrl = appendQuery(url, qsUrl);
      } else {
        qsUrl = qs.stringify({
          ...newFilters
        });
        resultUrl = appendQuery(url, qsUrl);
      }
      return {
        ...state,
        filters,
        endTime,
        startTime,
        resultUrl,
        url,
        isFetching: false
      };
    }
    case SET_URL:
    {
      let url = action.value;
      let {endTime, startTime, filters } = state;
      let resultUrl;
      if (url) {
        if (startTime) {
          let qsUrl = qs.stringify({
            ...filters,
            startTime,
            endTime
          });
          resultUrl = appendQuery(url, qsUrl);
        } else {
          let qsUrl = qs.stringify({
            ...filters
          });
          resultUrl = appendQuery(url, qsUrl);
        }

      }
      return {
        ...state,
        filters,
        endTime,
        startTime,
        resultUrl,
        url,
        isFetching: false
      };
    }
    case SET_DATE:
    {
      let startDate = action.startDate;
      let endDate = action.endDate;

      const { url, filters } = state;
      let startTime, endTime
      if (endDate) {
        startTime = state.startTime;
        endTime = reshapeDate(endDate._d);
      }
      else {
        startTime = reshapeDate(startDate._d);
        endTime = reshapeDate(startDate._d); 
      }
      let qsUrl = qs.stringify({
        ...filters,
        startTime,
        endTime
      });
      let resultUrl = appendQuery(url, qsUrl);
      return {
        ...state,
        filters,
        endTime,
        startTime,
        resultUrl,
        url,
        isFetching: false
      };
    }
    case GET_OPTIONS:
    {
      const base_url = 'http://taj-mock-assets.s3.amazonaws.com/';
      const option_url = action.url;
      let url = base_url + option_url;
      console.log(url);
      getOptionsFromApiAsync(url)
      .then(result => 
        {
          console.log(result)
          let filter_options = result;
          const name = action.option;
          let options = state.options;
          options[name] = filter_options;
          return  {
            ...state,
            options
          }
        })
      .catch(err => console.log(err));  
      return state;
    }
    default:
      return state;
  }
};

export default dashboard;
