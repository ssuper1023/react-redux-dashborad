from flask import Flask
from flask import Response
from flask_cors import CORS

import json
from os import path
import pandas as pd
import numpy as np

from taj import df_to_json


app = Flask(__name__)
CORS(app)

here = path.abspath(path.dirname(__file__))
print(here)

data_path = path.join(here, 'data', 'iris.csv')

iris = pd.read_csv(data_path)

# mpg_path = path.join(here, 'data', 'mpg.csv')
# mpg = pd.read_csv(mpg_path)

db_path = path.join(here, 'data', 'db.json')

with open(db_path) as f:
    db_json = json.loads(f.read())


def _do_iris_multicolumns(df_iris):
    """
    The idea here is to stuff iris dataframe to create a 3-level columns multiIndex
    """
    df = df_iris.copy()
    np.random.seed(12345)
    choice = np.random.choice

    # Randomly create some discrete data
    #
    df['location'] = choice(['north', 'south'], len(df))
    df['date'] = choice(np.arange(np.datetime64('2018-01'), np.datetime64('2018-04')), len(df))

    # Now, rearrange the table
    #
    ndf = pd.pivot_table(df,
                         index=['species', 'location'],
                         columns=['date'],
                         aggfunc=[min,max]).reorder_levels([1,2,0], axis=1)
    ndf.columns = ndf.columns.sortlevel()[0]
    ndf.columns.names = ['variable','date','stats']

    return ndf


iris_multicols = _do_iris_multicolumns(iris)


@app.route("/iris_multicols")
def iris_multicols_view():
    global iris_multicols

    response = Response(
        response=df_to_json(iris_multicols),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route("/original")
def original():
    global db_json
    response = Response(
        response=json.dumps(db_json),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route("/iris")
def iris_view():
    global iris

    response = Response(
        response=df_to_json(iris, filterFields=['species','petal_length']),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route("/mpg")
def mpg_view():
    global mpg

    response = Response(
        response=df_to_json(mpg),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route("/dfmulti")
def dfmulti():

    global iris
    gdf = iris.groupby('species').agg(['min', 'max','mean'])

    response = Response(
        response=df_to_json(gdf),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route("/dfmulti2")
def dfmulti2():

    global iris
    iris['location'] = np.random.choice(['north', 'south'], iris.shape[0])
    gdf = iris.groupby(['species', 'location']).agg(['min', 'max'])

    response = Response(
        response=df_to_json(gdf),
        status=200,
        mimetype='application/json'
    )
    return response


if __name__ == '__main__':
    app.run(threaded=True, debug=True)
